package ru.mail.maks825;

public class Factorial {
    public static void main(String[] args) {

        System.out.println("Задание №3");
        System.out.println("Посчитайте факториал 20, и выведите результат в консоль.");

        long n = 20;
        System.out.printf("Факториал числа %d равен: %d", n, calculateFactorial(n));
    }

    private static long calculateFactorial(long factorial) {
        return factorial == 1 ? 1 : factorial * calculateFactorial(factorial - 1);
    }
}

