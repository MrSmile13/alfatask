package ru.mail.maks825;

import java.io.*;
import java.util.*;

public class NumberSorter {
    public static void main(String[] args) {
        System.out.println("Задание №2");
        System.out.println("Создайте текстовый файл, в котором есть значения от 0 до 20 в произвольном порядке (перемешаны).");
        System.out.println("Значения указаны через запятую без пробелов.");
        System.out.println("\t- Прочитайте файл, отфильтруйте значения по возрастанию и выведите результат в консоль.");
        System.out.println("\t- Прочитайте файл, отфильтруйте значения по убыванию и выведите результат вконсоль.\n");

        String numberInFile = "numbers.txt";
        File file = new File(numberInFile);

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String[] numbersString = reader.readLine().trim().split(",");

            ArrayList<Integer> numbersInt = new ArrayList<>();

            for (int i = 0; i < numbersString.length; i++) numbersInt.add(Integer.parseInt(numbersString[i]));
            System.out.print("Числа в файле: " + numbersInt.toString().replaceAll("^\\[|\\]", ""));
            System.out.println();

            Collections.sort(numbersInt);
            System.out.print("Числа, отсортированные по возрастанию: "
                    + numbersInt.toString().replaceAll("^\\[|\\]", ""));
            System.out.println();

            Collections.reverse(numbersInt);
            System.out.print("Числа, отсортированные по убыванию: "
                    + numbersInt.toString().replaceAll("^\\[|\\]", ""));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
